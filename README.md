# Gatsby-source-facebook

A Gatsby source plugin for sourcing data into your Gatsby application from Facebooks graph API.

## Install

`npm install --save gatsby-source-facebook`

## How to use:

```javascript
// In your gatsby-config.js
module.exports = {
  plugins: [
    {
      resolve: `gatsby-source-facebook`,
      options: {
        places: [`${facebookPageID}`], // Can be either a numeric ID or the URL ID
        params: {
          fields: 'hours, posts { message, created_time }', // See Facebooks API to see what you can query for
        },
        key: process.env.FACEBOOK_GRAPH_TOKEN, // You will need to create a Facebook application and go through review in order to get an API token.
      },
    },
  ],
}
```
