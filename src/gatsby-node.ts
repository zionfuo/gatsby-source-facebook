import * as crypto from 'crypto'
import * as graph from 'fbgraph'

const getData = (from: string, params = {}) => {
  return new Promise((resolve, reject) => {
    graph.get(from, params, (err: any, res: any) => {
      if (err) {
        return reject(err)
      }
      if (!res) {
        return reject(new Error('Response is empty!'))
      }
      resolve(res)
    })
  })
}

const titleCase = (str: string): string => {
  return str.replace(/\w\S*/g, txt => {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  })
}

export interface PluginOptions {
  key: string
  places: string[]
  params?: object
}

export const sourceNodes = async (
  { boundActionCreators, createNodeId }: any,
  pluginOptions: PluginOptions,
) => {
  graph.setAccessToken(pluginOptions.key)

  const { createNode } = boundActionCreators

  const processData = (place: string, type: string, data: any) => {
    if (type === 'Hours') {
      data = { hours: Object.entries(data) }
    }

    console.log(type, data)
    if (typeof data !== 'object') {
      console.error('Data is not an object, skipping!')
      return
    }
    const nodeId = createNodeId(`${place}_${type}_${data.id}`)
    const nodeContent = JSON.stringify(data)
    const nodeContentDigest = crypto
      .createHash('md5')
      .update(nodeContent)
      .digest('hex')

    const nodeData = Object.assign({}, data, {
      id: nodeId,
      parent: null,
      children: [],
      internal: {
        type: `Facebook${type}`,
        content: nodeContent,
        contentDigest: nodeContentDigest,
      },
    })

    return nodeData
  }

  for (const place of pluginOptions.places) {
    const data = await getData(`${place}`, pluginOptions.params)

    console.log(data)
    Object.entries(data).forEach(([type, typeData]) => {
      type = titleCase(type)
      let arr: any[]
      if (Array.isArray(typeData.data)) {
        arr = typeData.data
      } else {
        arr = [typeData]
      }
      arr.forEach(thing => {
        const nodeData = processData(place, type, thing)
        if (nodeData) {
          return createNode(nodeData)
        }
      })
    })
    /*
    await Promise.all(
      Object.entries(data).map(([type, typeData]) => {
        type = titleCase(type)
        Object.entries(typeData).map(([key, value]) => {
          if (key === 'data' && Array.isArray(value)) {
            value.forEach(v => {
              const nodeData = processData(type, key, v)
              return createNode(nodeData)
            })
          } else {
            const nodeData = processData(type, key, value)
            return createNode(nodeData)
          }
        })
      }),
    )
    */
  }
}
